// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.13;

contract ContractWithMoney {
    // With payable modifier, the contract can receive money when deployed.
    // The amount is the value of the transaction.
    constructor() payable {}

    // This method can receive money for the contract when called by msg.sender
    // The amount is the value of the transaction.
    function deposit() public payable {}

    // Send all balances of this contract to whomever calls this method.
    function withdraw() public {
        payable(msg.sender).transfer(address(this).balance);
    }

    function balance() public view returns (uint256) {
        return address(this).balance;
    }

}