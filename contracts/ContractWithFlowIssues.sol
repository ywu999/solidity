// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.13;

contract ContractWithFlowIssues {
    uint8 private balance;

    function decrease() public {
        balance--;
    }

    function increase() public {
        balance++;
    }

    function setBalance(uint8 _balance) public {
        balance = _balance;
    }

    function getBalance() public view returns (uint8) {
        return balance;
    }
}

// Knowledge base
// 
// The integer over flow and under flow vunerability were fixed using solidity 
// version>0.8.0 . The edgy scenario will throw a panic error, and the
// transaction will be reverted. 
//
// It was a problem if any one uses a lower version. The developer had to use SafeMath
// library to handle the situation. 
//